package aviasales

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"gitlab.com/sarbash/locahints/pkg/search"
)

const endpoint = "https://places.aviasales.ru/v2/places.json"

// Service provides location search operations.
type Service struct {
	client *http.Client
}

// NewService creates a search service for the aviasales location hints provider.
func NewService() *Service {
	return &Service{
		client: &http.Client{
			Timeout: 3 * time.Second,
		},
	}
}

// Search returns search results.
func (s *Service) Search(ctx context.Context, r search.Request) search.Results {
	results := search.Results{}

	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		log.Println("During creating new request for the term '", r.Term, "':", err)
		return results
	}

	q := req.URL.Query()
	q.Set("term", r.Term)
	q.Set("locale", r.Locale)
	for _, t := range r.Types {
		q.Add("types[]", t)
	}
	req.URL.RawQuery = q.Encode()

	resp, err := s.client.Do(req.WithContext(ctx))
	if err != nil {
		log.Println("During performing request for the term '", r.Term, "':", err)
		return results
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		log.Println("Request for the term '", r.Term, "' failed with the status:", resp.Status)
		return results
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("During reading response body for the term '", r.Term, "':", err)
		return results
	}

	hs := hints{}
	err = json.Unmarshal(body, &hs)
	if err != nil {
		log.Println("During deserialising response for the term '", r.Term, "':", err)
		return results
	}
	if len(hs) == 0 {
		return results
	}

	res := hs.toResultFormat()
	results, err = json.Marshal(res)
	if err != nil {
		log.Println("Unexpected error:", err)
	}

	return results
}

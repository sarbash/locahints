package aviasales

import (
	"context"
	"net/http"
	"net/url"
	"testing"
	"time"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/sarbash/locahints/pkg/search"
)

func TestNewService(t *testing.T) {
	svc := &Service{
		client: &http.Client{
			Timeout: 3 * time.Second,
		},
	}

	testingSvc := NewService()

	assert.Equal(t, *svc, *testingSvc)
}

func TestSearch(t *testing.T) {
	assert := assert.New(t)

	s := &Service{
		client: http.DefaultClient,
	}

	r := search.Request{
		Term:   "Россия",
		Locale: "ru",
		Types: []string{
			"city",
			"airport",
		},
	}

	resp := `[{"main_airport_name":null,"cases":{"da":"Москве","vi":"в Москву","tv":"Москвой","pr":"Москве","ro":"Москвы"},"state_code":null,"country_cases":null,"coordinates":{"lon":37.617633,"lat":55.755786},"country_code":"RU","code":"MOW","country_name":"Россия","name":"Москва","type":"city","index_strings":["defaultcity","defaultcity","maskava","maskva","mosca","moscou","moscova","moscovo","moscow","moscú","moskau","moskou","moskova","moskow","moskva","moskwa","moszkva","μόσχα","москва","нерезиновая","нерезиновая","нерезиновск","нерезиновск","понаехавск","понаехавск","մոսկվա","מוסקבה","مسکو","موسكو","मास्को","มอสโก","მოსკოვი","モスクワ","莫斯科","모스크바"],"weight":1006321},{"state_code":null,"cases":null,"city_name":"Москва","country_cases":null,"coordinates":{"lon":37.899494,"lat":55.414566},"country_code":"RU","code":"DME","country_name":"Россия","name":"Домодедово","type":"airport","index_strings":["aeroportul domodedovo moscova","defaultcity","domodedovo","domodedovo internasjonale flyplass","domodedovo luchthaven","domodedowo","domodiedowo","letališče moskva domodedovo","letisko moskva domodedovo","letiště moskva domodedovo","maskava","maskavas domodedovo lidosta","maskva","međunarodni aerodrom domodedovo","mosca","moscou","moscova","moscovo","moscovo - domodedovo","moscow","moscow domodedovo","moscow domodedovo airport","moscow domodedovos flygplats","moscú","moskau","moskou","moskova","moskova domodedovon lentokenttä","moskow","moskva","moskva-domodedovo internationale lufthavn","moskwa","moszkva","moszkva domodedovo repülőtér","zračna luka moscow domodedovo","μόσχα","μόσχα ντομοντέντοβο αεροδρόμιο","домодедово","домодєдово","летище москва домодедово","москва","нерезиновая","нерезиновск","понаехавск","դոմոդեդովո","մոսկվա","מוסקבה","נמל התעופה מוסקבה דומודאדובו","فرودگاه بین‌المللی دوموده‌دوو","مسکو","مطار دوموديدوڤو موسكو","موسكو","मास्को","मास्को दोमोदेदोवो हवाई अड्डा","มอสโก","สนามบินโดโมเดโดโว","დამაძედოვოს აეროპორტი","მოსკოვი","モスクワ","多莫杰多沃机场","莫斯科","모스크바"],"city_code":"MOW","weight":613709,"city_cases":{"da":"Москве","vi":"в Москву","tv":"Москвой","pr":"Москве","ro":"Москвы"}}]`

	results := search.Results(`[{"slug":"MOW","subtitle":"Россия","title":"Москва"},{"slug":"DME","subtitle":"Москва","title":"Домодедово"}]`)

	// Test good response.

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	expectedQuery := url.Values{
		"term":    []string{r.Term},
		"locale":  []string{r.Locale},
		"types[]": []string{r.Types[0], r.Types[1]},
	}
	httpmock.RegisterResponderWithQuery(http.MethodGet, endpoint, expectedQuery, httpmock.NewStringResponder(http.StatusOK, resp))

	ctx := context.Background()

	res := s.Search(ctx, r)
	assert.Equal(search.Results(results), res)

	// Test failed request.

	r.Locale = "en"

	res = s.Search(ctx, r)
	assert.Empty(res)

	// Test nil response body.

	expectedQuery = url.Values{
		"term":    []string{r.Term},
		"locale":  []string{r.Locale},
		"types[]": []string{r.Types[0], r.Types[1]},
	}
	httpmock.RegisterResponderWithQuery(http.MethodGet, endpoint, expectedQuery, httpmock.NewBytesResponder(http.StatusOK, nil))

	res = s.Search(ctx, r)
	assert.Empty(res)

	// Test empty response body.

	httpmock.RegisterResponderWithQuery(http.MethodGet, endpoint, expectedQuery, httpmock.NewStringResponder(http.StatusOK, "[]"))

	res = s.Search(ctx, r)
	assert.Empty(res)
}

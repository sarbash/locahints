package aviasales

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestToResultFormat(t *testing.T) {
	hs := hints{
		{
			CityName:    "",
			Code:        "MOW",
			CountryName: "Россия",
			Name:        "Москва",
			Type:        "city",
		},
		{
			CityName:    "Бугульма",
			Code:        "FAKE",
			CountryName: "Россия",
			Name:        "Музей",
			Type:        "museum",
		},
		{
			CityName:    "Москва",
			Code:        "DME",
			CountryName: "Россия",
			Name:        "Домодедово",
			Type:        "airport",
		},
	}

	res := hs.toResultFormat()

	assert := assert.New(t)
	assert.Len(res, 2)

	assert.Equal(res[0].Slug, hs[0].Code)
	assert.Equal(res[0].Subtitle, hs[0].CountryName)
	assert.Equal(res[0].Title, hs[0].Name)

	assert.Equal(res[1].Slug, hs[2].Code)
	assert.Equal(res[1].Subtitle, hs[2].CityName)
	assert.Equal(res[1].Title, hs[2].Name)
}

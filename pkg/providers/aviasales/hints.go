package aviasales

import (
	"log"

	"gitlab.com/sarbash/locahints/pkg/search"
)

// hint defines the necessary part of the current properties of a location hint from aviasales.
type hint struct {
	CityName    string `json:"city_name"`
	Code        string `json:"code"`
	CountryName string `json:"country_name"`
	Name        string `json:"name"`
	Type        string `json:"type"`
}

// hints represents collection of location hints returning from aviasales.
type hints []hint

func (hh hints) toResultFormat() []search.Result {
	rr := []search.Result{}
	for _, h := range hh {
		switch h.Type {
		case "airport":
			rr = append(rr, search.Result{
				Slug:     h.Code,
				Subtitle: h.CityName,
				Title:    h.Name,
			})
		case "city":
			rr = append(rr, search.Result{
				Slug:     h.Code,
				Subtitle: h.CountryName,
				Title:    h.Name,
			})
		default:
			log.Println("WARNING! Unknown hint type:", h.Type)
		}
	}

	return rr
}

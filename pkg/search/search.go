package search

import (
	"bytes"
	"crypto/sha1"
)

// Request defines the properties of a search to be performed.
type Request struct {
	Term   string
	Locale string
	Types  []string
}

// Result defines the properties of a search result.
type Result struct {
	Slug     string `json:"slug"`
	Subtitle string `json:"subtitle"`
	Title    string `json:"title"`
}

// Results defines search results to be returned.
type Results []byte

// Hash returns the hashsum string of the request to use it as a key for storage in a cache.
func (r *Request) Hash() string {
	b := bytes.Buffer{}
	b.WriteString(r.Term)
	b.WriteString(r.Locale)
	for _, s := range r.Types {
		b.WriteString(s)
	}

	sum := sha1.Sum(b.Bytes())

	return string(sum[:])
}

package search

import (
	"context"
	"errors"
	"log"
	"time"
)

// Cache provides access to the cached searches.
type Cache interface {
	GetSearch(ctx context.Context, key string) Results
	SaveSearch(ctx context.Context, key string, results Results) error
}

// Provider provides location search operations of the particular 3rd-party provider.
type Provider interface {
	Search(context.Context, Request) Results
}

// Service provides location search operations.
type Service interface {
	Search(context.Context, Request) Results
}
type service struct {
	provider Provider
	cache    Cache
}

// NewService creates a search service with the necessary dependencies.
func NewService(p Provider, c Cache) (Service, error) {
	if p == nil {
		return nil, errors.New("Provider cannot be nil")
	}
	if c == nil {
		return nil, errors.New("Cache cannot be nil")
	}

	return &service{
		provider: p,
		cache:    c,
	}, nil
}

// Search returns search results.
func (s *service) Search(ctx context.Context, r Request) Results {
	key := r.Hash()
	log.Printf("Calculated request key: %x", key)

	cached := s.cache.GetSearch(ctx, key)
	if len(cached) > 0 {
		log.Printf("Using the cached version of the query with the key %x", key)
		return cached
	}

	if d, _ := ctx.Deadline(); time.Now().After(d) {
		return Results{}
	}

	results := s.provider.Search(ctx, r)
	if len(results) > 0 {
		log.Printf("Caching the query with the key %x", key)
		s.cache.SaveSearch(ctx, key, results)
	}

	return results
}

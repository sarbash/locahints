package search_test

import (
	"context"
	"errors"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sarbash/locahints/mock"
	"gitlab.com/sarbash/locahints/pkg/search"
)

func TestNewService(t *testing.T) {
	s, err := search.NewService(&mock.Provider{}, &mock.Cache{})
	assert.NotNil(t, s)
	assert.NoError(t, err)

	testCases := []struct {
		name     string
		provider search.Provider
		cache    search.Cache
		service  search.Service
		err      error
	}{
		{
			name: "Nil provider and cache",
			err:  errors.New("Provider cannot be nil"),
		},
		{
			name:  "Nil provider",
			cache: &mock.Cache{},
			err:   errors.New("Provider cannot be nil"),
		},
		{
			name:     "Nil cache",
			provider: &mock.Provider{},
			err:      errors.New("Cache cannot be nil"),
		},
		{
			name:     "Good service",
			provider: &mock.Provider{},
			cache:    &mock.Cache{},
			service:  s,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s, err := search.NewService(tc.provider, tc.cache)
			assert.Equal(t, tc.service, s)
			assert.Equal(t, tc.err, err)
		})
	}
}

func TestSearch(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	testCases := []struct {
		name     string
		request  search.Request
		provider search.Provider
		cache    search.Cache
		results  search.Results
	}{
		{
			name: "Using cached result",
			request: search.Request{
				Term:   "Москва",
				Locale: "en",
				Types:  []string{"city", "airport"},
			},
			provider: &mock.Provider{},
			cache: &mock.Cache{
				GetSearchFn: func(key string) search.Results {
					if fmt.Sprintf("%x", key) != "27503ab966316938b8f5d3c839aea70be43c2728" {
						t.Fatalf("unexpected key: %x", key)
					}
					return search.Results(`"[{"slug":"MOW","subtitle":"Russia","title":"Moscow"},{"slug":"DME","subtitle":"Moscow","title":"Moscow Domodedovo Airport"},{"slug":"VKO","subtitle":"Moscow","title":"Vnukovo Airport"},{"slug":"SVO","subtitle":"Moscow","title":"Sheremetyevo International Airport"},{"slug":"ZIA","subtitle":"Moscow","title":"Zhukovsky International Airport"}]`)
				},
				SaveSearchFn: func(key string, results search.Results) error {
					return nil
				},
			},
			results: search.Results(`"[{"slug":"MOW","subtitle":"Russia","title":"Moscow"},{"slug":"DME","subtitle":"Moscow","title":"Moscow Domodedovo Airport"},{"slug":"VKO","subtitle":"Moscow","title":"Vnukovo Airport"},{"slug":"SVO","subtitle":"Moscow","title":"Sheremetyevo International Airport"},{"slug":"ZIA","subtitle":"Moscow","title":"Zhukovsky International Airport"}]`),
		},
		{
			name: "Using provider service",
			request: search.Request{
				Term:   "Набережные Челны",
				Locale: "ru",
				Types:  []string{"city", "airport"},
			},
			provider: &mock.Provider{
				SearchFn: func(request search.Request) search.Results {
					return search.Results(`[{"slug":"NBC","subtitle":"Россия","title":"Набережные Челны (Нижнекамск)"}]`)
				},
			},
			cache: &mock.Cache{
				GetSearchFn: func(key string) search.Results {
					return nil
				},
				SaveSearchFn: func(key string, results search.Results) error {
					return nil
				},
			},
			results: search.Results(`[{"slug":"NBC","subtitle":"Россия","title":"Набережные Челны (Нижнекамск)"}]`),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			s, err := search.NewService(tc.provider, tc.cache)
			assert.NotNil(t, s)
			assert.NoError(t, err)

			results := s.Search(ctx, tc.request)
			assert.Equal(t, tc.results, results)
		})
	}
}

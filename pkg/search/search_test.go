package search_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/sarbash/locahints/pkg/search"
)

func TestHash(t *testing.T) {
	testCases := []struct {
		name    string
		request search.Request
		result  string
	}{
		{
			name: "Case 1",
			request: search.Request{
				Term:   "Москва",
				Locale: "en",
				Types:  []string{"city"},
			},
			result: "f5bfdc99762a0e72bc5316f570a04b97f908a50f",
		},
		{
			name: "Case 2",
			request: search.Request{
				Term:   "Нижнекамск",
				Locale: "ru",
				Types:  []string{"airport"},
			},
			result: "6192dd793826468f91b167da1b5be8f8a02e6727",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res := tc.request.Hash()
			assert.Equal(t, tc.result, fmt.Sprintf("%x", res))
		})
	}
}

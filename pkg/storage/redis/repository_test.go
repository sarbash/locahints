package redis_test

import (
	"context"
	"os"
	"testing"

	"gitlab.com/sarbash/locahints/pkg/search"
	"gitlab.com/sarbash/locahints/pkg/storage/redis"

	"github.com/alicebob/miniredis"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var dbVar = "REDIS_DB"

func TestNewStorage(t *testing.T) {
	r := miniredis.NewMiniRedis()

	testCases := []struct {
		name       string
		addr       string
		skipEnvVar bool
		err        string
	}{
		{
			name:       "Without envvar",
			skipEnvVar: true,
		},
		{
			name: "Using envvar",
			addr: "localhost:1234",
		},
		{
			name:       "Connection failure",
			addr:       "localhost:2345",
			skipEnvVar: true,
			err:        "dial tcp [::1]:1234: connect: connection refused",
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			addr := tc.addr
			if len(addr) == 0 {
				addr = redis.DefaultDB
			}

			if !tc.skipEnvVar {
				err := os.Setenv(dbVar, addr)
				require.NoError(t, err)
			}

			err := r.StartAddr(addr)
			require.NoError(t, err)

			s, err := redis.NewStorage()
			r.Close()

			if len(tc.err) == 0 {
				assert.NoError(t, err)
				assert.NotNil(t, s)
			} else {
				assert.Equal(t, tc.err, err.Error())
				assert.Nil(t, s)
			}
		})
	}
}

func TestGetSearch(t *testing.T) {
	addr := "localhost:5678"

	err := os.Setenv(dbVar, addr)
	require.NoError(t, err)

	r := miniredis.NewMiniRedis()
	err = r.StartAddr(addr)
	require.NoError(t, err)
	defer r.Close()

	s, err := redis.NewStorage()
	require.NoError(t, err)
	require.NotNil(t, s)

	ctx := context.Background()

	testCases := []struct {
		name string
		key  string
		val  search.Results
	}{
		{
			name: "Using empty key",
		},
		{
			name: "Using non-existent key",
			key:  "xyz",
			val:  search.Results{},
		},
		{
			name: "Using cached key",
			key:  "test_key",
			val:  search.Results("test_value"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if len(tc.val) > 0 {
				err := r.Set(tc.key, string(tc.val))
				require.NoError(t, err)
			}

			val := s.GetSearch(ctx, tc.key)
			assert.Equal(t, tc.val, val)
		})
	}
}

func TestSaveSearch(t *testing.T) {
	addr := "localhost:6789"

	err := os.Setenv(dbVar, addr)
	require.NoError(t, err)

	r := miniredis.NewMiniRedis()
	err = r.StartAddr(addr)
	require.NoError(t, err)

	s, err := redis.NewStorage()
	require.NoError(t, err)
	require.NotNil(t, s)

	ctx := context.Background()

	testCases := []struct {
		name string
		key  string
		val  search.Results
		err  string
	}{
		{
			name: "Using empty key",
		},
		{
			name: "Using empty value",
			key:  "xyz",
			val:  search.Results{},
		},
		{
			name: "Using valid key and value",
			key:  "test_key",
			val:  search.Results("test_value"),
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			err := s.SaveSearch(ctx, tc.key, tc.val)

			if len(tc.err) > 0 {
				assert.Equal(t, tc.err, err.Error())
			}

			if len(tc.key) > 0 {
				val := s.GetSearch(ctx, tc.key)
				assert.Equal(t, tc.val, val)
			}
		})
	}

	// Test connection abortion.
	r.Close()

	err = s.SaveSearch(ctx, "test_key", search.Results("test_val"))
	assert.Error(t, err)
	assert.Equal(t, "EOF", err.Error())
}

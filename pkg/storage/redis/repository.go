package redis

import (
	"context"
	"log"
	"os"

	"github.com/go-redis/redis"
	"gitlab.com/sarbash/locahints/pkg/search"
)

const (
	// DefaultDB will be used for connection to the Redis if no value was provided by the envvar.
	DefaultDB = "127.0.0.1:6379"
)

const (
	dbVar = "REDIS_DB"
)

// Storage stores search data in Redis.
type Storage struct {
	db *redis.Client
}

// NewStorage constructs new storage object. It initializes a connection to Redis and returns error if it fails.
func NewStorage() (*Storage, error) {
	addr := os.Getenv(dbVar)
	if len(addr) == 0 {
		addr = DefaultDB
	}

	db := redis.NewClient(&redis.Options{Addr: addr})

	err := db.Ping().Err()
	if err != nil {
		log.Println("Failed to establish connection to Redis storage:", err)
		return nil, err
	}

	return &Storage{
		db: db,
	}, nil
}

// GetSearch looks up for the cached search using the key provided.
func (s *Storage) GetSearch(ctx context.Context, key string) search.Results {
	if len(key) == 0 {
		log.Println("WARNING! Empty key for lookup was provided. Skipping search")
		return nil
	}

	res := make(chan search.Results, 1)
	go func() {
		val, err := s.db.Get(key).Result()
		if err != nil {
			log.Printf("Failed to get the key %x: %s", key, err)
		}
		res <- search.Results(val)
	}()

	select {
	case <-ctx.Done():
		return nil
	case val := <-res:
		return val
	}
}

// SaveSearch caches search results into the storage under the key provided.
// It returns nil if the operation succeeded or an error if occurred.
func (s *Storage) SaveSearch(ctx context.Context, key string, results search.Results) error {
	if len(key) == 0 {
		log.Println("WARNING! Empty search key for storage was provided. The search will not be cached")
		return nil
	}

	err := make(chan error, 1)
	go func() {
		err <- s.db.Set(key, string(results), 0).Err()
	}()

	select {
	case <-ctx.Done():
		e := ctx.Err()
		log.Printf("During saving the search with the key %x: %s", key, e)
		return e
	case e := <-err:
		if e != nil {
			log.Printf("Failed to save the search with the key %x: %s", key, e)
		}
		return e
	}
}

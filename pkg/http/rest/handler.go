package rest

import (
	"context"
	"log"
	"net/http"
	"net/url"
	"time"

	"gitlab.com/sarbash/locahints/pkg/search"
)

// HandlingTimeout limits incoming requests processing time to the limit specified.
var HandlingTimeout = 3 * time.Second

// RegisterHandlers registers handlers for the requests to the service REST API.
func RegisterHandlers(s search.Service) {
	http.HandleFunc("/status", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("OK"))
	})
	http.HandleFunc("/places", doSearch(s))
	return
}

func doSearch(s search.Service) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")

		if r.Method != http.MethodGet {
			log.Println("Wrong HTTP-method for the request was used:", r.Method)
			w.Header().Set("Allow", http.MethodGet)
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		if q, err := url.QueryUnescape(r.RequestURI); err == nil {
			log.Println("Request URI:", q)
		}

		if err := r.ParseForm(); err != nil {
			log.Printf("During parsing query params: %s (%s)", err, r.RequestURI)
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if len(r.Form) == 0 {
			log.Println("No params were provided in the request")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		req := search.Request{
			Term:   r.Form.Get("term"),
			Locale: r.Form.Get("locale"),
		}
		for _, t := range r.Form["types[]"] {
			req.Types = append(req.Types, t)
		}

		if !isValid(req) {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		ctx, cancel := context.WithTimeout(context.Background(), HandlingTimeout)
		defer cancel()

		res := s.Search(ctx, req)
		if res == nil {
			log.Println("WARNING! Unexpected nil result from a search (must be empty, at least)")
			res = search.Results{}
		}

		w.Write(res)
	}
}

func isValid(r search.Request) bool {
	v := true
	if len(r.Term) == 0 {
		log.Println("Missing term in the request")
		v = false
	}
	if len(r.Locale) == 0 {
		log.Println("Missing locale in the request")
		v = false
	}
	if len(r.Types) == 0 {
		log.Println("Missing types of places to search in the request")
		v = false
	}

	return v
}

package rest_test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/sarbash/locahints/mock"
	"gitlab.com/sarbash/locahints/pkg/http/rest"
	"gitlab.com/sarbash/locahints/pkg/search"
)

func TestRegisterHandlers(t *testing.T) {
	response := `"[{"slug":"MOW","subtitle":"Russia","title":"Moscow"},{"slug":"DME","subtitle":"Moscow","title":"Moscow Domodedovo Airport"},{"slug":"VKO","subtitle":"Moscow","title":"Vnukovo Airport"},{"slug":"SVO","subtitle":"Moscow","title":"Sheremetyevo International Airport"},{"slug":"ZIA","subtitle":"Moscow","title":"Zhukovsky International Airport"}]`

	//  We will use the cached result because it doesn't matter here
	//  whether we get a result from the cache or by querying a hints provider.
	p := &mock.Provider{}
	c := &mock.Cache{
		GetSearchFn: func(key string) search.Results {
			if fmt.Sprintf("%x", key) != "27503ab966316938b8f5d3c839aea70be43c2728" {
				t.Fatalf("unexpected key: %x", key)
			}
			return search.Results(response)
		},
		SaveSearchFn: func(key string, results search.Results) error {
			return nil
		},
	}

	s, err := search.NewService(p, c)
	require.NoError(t, err)
	require.NotNil(t, s)

	rest.RegisterHandlers(s)

	// Test /status route.

	req := httptest.NewRequest(http.MethodGet, "http://example.com/status", nil)
	w := httptest.NewRecorder()

	http.DefaultServeMux.ServeHTTP(w, req)

	resp := w.Result()
	body, _ := ioutil.ReadAll(resp.Body)

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.Equal(t, "OK", string(body))

	// Test /places route.

	endpoint := "http://example.com/places?term=Москва&locale=en&types[]=city&types[]=airport"

	testCases := []struct {
		name        string
		reqMethod   string
		endpoint    string
		respStatus  int
		respBody    []byte
		allowHeader []string // for http method test
	}{
		{
			name:       "Using valid request",
			reqMethod:  http.MethodGet,
			endpoint:   endpoint,
			respStatus: http.StatusOK,
			respBody:   []byte(response),
		},
		{
			name:        "Using wrong HTTP method",
			reqMethod:   http.MethodPost,
			endpoint:    endpoint,
			respStatus:  http.StatusMethodNotAllowed,
			respBody:    []byte{},
			allowHeader: []string{http.MethodGet},
		},
		{
			name:       "Using malformed query",
			reqMethod:  http.MethodGet,
			endpoint:   "http://example.com/places?term=%Москва&locale=en&types[]=city&types[]=airport",
			respStatus: http.StatusBadRequest,
			respBody:   []byte{},
		},
		{
			name:       "Using parameterless request",
			reqMethod:  http.MethodGet,
			endpoint:   "http://example.com/places",
			respStatus: http.StatusBadRequest,
			respBody:   []byte{},
		},
		{
			name:       "Missing one param",
			reqMethod:  http.MethodGet,
			endpoint:   "http://example.com/places?term=Москва&types[]=city&types[]=airport",
			respStatus: http.StatusBadRequest,
			respBody:   []byte{},
		},
		{
			name:       "Missing two params",
			reqMethod:  http.MethodGet,
			endpoint:   "http://example.com/places?term=Москва",
			respStatus: http.StatusBadRequest,
			respBody:   []byte{},
		},
		{
			name:       "Missing term in query",
			reqMethod:  http.MethodGet,
			endpoint:   "http://example.com/places?locale=en&types[]=city&types[]=airport",
			respStatus: http.StatusBadRequest,
			respBody:   []byte{},
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			req := httptest.NewRequest(tc.reqMethod, tc.endpoint, nil)
			w := httptest.NewRecorder()

			http.DefaultServeMux.ServeHTTP(w, req)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, tc.respStatus, resp.StatusCode)
			assert.Equal(t, tc.respBody, body)
			if resp.StatusCode == http.StatusMethodNotAllowed {
				assert.Equal(t, tc.allowHeader, resp.Header["Allow"])
			}
		})
	}

	// Test simulation of malformed provider.
	// Hints provider should return an empty result at least, not nil.
	// The handler logs misbehavior and fixes a result so the provider implementation can be fixed.

	p = &mock.Provider{
		SearchFn: func(request search.Request) search.Results {
			return nil
		},
	}
	c = &mock.Cache{
		GetSearchFn: func(key string) search.Results {
			return nil
		},
		SaveSearchFn: func(key string, results search.Results) error {
			return nil
		},
	}

	s, err = search.NewService(p, c)
	require.NoError(t, err)
	require.NotNil(t, s)

	http.DefaultServeMux = http.NewServeMux()

	rest.RegisterHandlers(s)

	req = httptest.NewRequest(http.MethodGet, endpoint, nil)
	w = httptest.NewRecorder()

	http.DefaultServeMux.ServeHTTP(w, req)

	resp = w.Result()
	body, _ = ioutil.ReadAll(resp.Body)

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.Empty(t, body)
}

package mock

import (
	"context"

	"gitlab.com/sarbash/locahints/pkg/search"
)

// Cache represents a mock implementation of search.Cache.
type Cache struct {
	GetSearchFn  func(key string) search.Results
	SaveSearchFn func(key string, results search.Results) error
}

// GetSearch invokes the mock implementation.
func (c *Cache) GetSearch(ctx context.Context, key string) search.Results {
	return c.GetSearchFn(key)
}

// SaveSearch invokes the mock implementation.
func (c *Cache) SaveSearch(ctx context.Context, key string, results search.Results) error {
	return c.SaveSearchFn(key, results)
}

// Provider represents a mock implementation of search.Provider.
type Provider struct {
	SearchFn func(request search.Request) search.Results
}

// Search invokes the mock implementation.
func (p *Provider) Search(ctx context.Context, request search.Request) search.Results {
	return p.SearchFn(request)
}

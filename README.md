# LOCAHINTS (LOCAtion HINTS) [![pipeline status](https://gitlab.com/sarbash/locahints/badges/master/pipeline.svg)](https://gitlab.com/sarbash/locahints/commits/master) [![coverage report](https://gitlab.com/sarbash/locahints/badges/master/coverage.svg)](https://gitlab.com/sarbash/locahints/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/sarbash/locahints)](https://goreportcard.com/report/gitlab.com/sarbash/locahints)

This is the location hints service.

**Table of contents:**

* [Project description](#project-description)
* [REST API description](#rest-api-description)
* [How to install](#how-to-install)
  * [Docker way](#docker-way)
  * [Building from sources](#building-from-sources)

## Project description

This is the test work having the goal to convert a response of third-party service to the format appropriated for feeding to services.
Currently, it has the support for only one hints provider - [Aviasales](https://www.aviasales.ru/).

As the base for project structuring was used ["GopherCon 2018 - How Do You Structure Your Go Apps?"](https://about.sourcegraph.com/go/gophercon-2018-how-do-you-structure-your-go-apps) article.

## REST API description

The `locahints` service exposes REST API for interaction. In its core lays the principle of reliability. Hints serve as an autocompletion option so they have to be provided in a very limited timeframe. By default, usual service response time shouldn't exceed some 3 seconds. Actually, it is more shorter. Any failures and errors are logged out but a service consumer always receives valid response which might be empty in the case of error.

The API makes available the following endpoints:

| **Method** | **Route** |  **Description** |
| ---------- | --------- | ---------------- |
| GET        | `/status` | Serves for health check of the service. If the service is alive responses with "OK" plain text |
| GET        | `/places` | Serves for location hints requests. It's the main endpoint of the service |

The service accepts the following parameters. All parameters are mandatory:

| **Name**  | **Description** |
| --------- | --------------- |
| `term`    | Part or complete place name we are looking for |
| `locale`  | 2-chars code of the language for which the response should be returned |
| `types[]` | Types of places for which to lookup. The query may contain several these params |

**IMPORTANT NOTE!** Currently, the service supports only two types of `types[]`: **`airport`** and **`city`**.

The service responds with JSON-encoded payload.

An example of a request to the service:

```sh
GET localhost:8080/places?term=Москва&locale=en&types[]=city&types[]=airport
```

An example of the response to the request above:

```json
[
    {
        "slug":"MOW",
        "subtitle":"Russia",
        "title":"Moscow"
    },
    {
        "slug":"DME",
        "subtitle":"Moscow",
        "title":"Moscow Domodedovo Airport"
    },
    {
        "slug":"VKO",
        "subtitle":"Moscow",
        "title":"Vnukovo Airport"
    },
    {
        "slug":"SVO",
        "subtitle":"Moscow",
        "title":"Sheremetyevo International Airport"
    },
    {
        "slug":"ZIA",
        "subtitle":"Moscow",
        "title":"Zhukovsky International Airport"
    }
]
```

The service returns the following HTTP status codes:

| **Status code** | **Description** |
| 200 | Returned in the case of success. The response body should contain json payload. However, be careful, as the list may be empty |
| 400 | Returned when something wrong with the request. Possible causes: invalid query string, missing param, empty param value |
| 405 | Returned when wrong HTTP method for the request was used |

In the case of an error the response body should be empty, nothing. Errors may be checked in logs.

## How to install

### Docker way

The preffered and easiest way is to use `Docker` and `docker-compose` utility.
Just pick up the `docker-compose.yml` from the root of the project and use it.
By default the service listens for incoming requests on the port 8080. This can be easily changed by modifying the `ports:` setting within the Compose file.
For example, if you want to change the port to 6060 make the line with ports values to looks like this:

Before:

```docker
...
    ports:
      - "8080:8080"
...
```

After:

```docker
...
    ports:
      - "8080:6060"
...
```

### Building from sources

If you want to build the service from the sources you need at least Go 1.11 to be installed.
Also, you have to be familiar with Go and its toolchain.
Currently, the service uses Redis database as the cache so you have to set up and run Redis for the service to work.
You need to point the service onto the Redis instance using `REDIS_DB` environment variable. Use `ip:port` or `hostname:port` format.

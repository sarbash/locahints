FROM alpine

RUN apk add --no-cache ca-certificates

COPY cmd/locahints/locahints /usr/bin/

EXPOSE 8080

WORKDIR /usr/bin

CMD [ "locahints" ]

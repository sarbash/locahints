package main

import (
	"log"
	"net/http"

	"gitlab.com/sarbash/locahints/pkg/http/rest"
	"gitlab.com/sarbash/locahints/pkg/providers/aviasales"
	"gitlab.com/sarbash/locahints/pkg/search"
	"gitlab.com/sarbash/locahints/pkg/storage/redis"
)

func main() {
	cache, err := redis.NewStorage()
	if err != nil {
		log.Fatalln(err)
	}
	provider := aviasales.NewService()

	svc, err := search.NewService(provider, cache)
	if err != nil {
		log.Fatalln(err)
	}

	// register HTTP handlers for incoming requests
	rest.RegisterHandlers(svc)

	log.Println("Start serving incoming requests on :8080")
	log.Fatalln(http.ListenAndServe(":8080", nil))
}

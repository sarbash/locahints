module gitlab.com/sarbash/locahints

require (
	github.com/alicebob/gopher-json v0.0.0-20180125190556-5a6b3ba71ee6 // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/jarcoal/httpmock v1.0.3
	github.com/onsi/ginkgo v1.8.0 // indirect
	github.com/onsi/gomega v1.5.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	github.com/yuin/gopher-lua v0.0.0-20190206043414-8bfc7677f583 // indirect
)
